

<div id="footer-bottom">

<!-- /// FOOTER-BOTTOM     ////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div class="container">
            <div class="row">
                <div class="span6" id="footer-bottom-widget-area-1">

                    <div class="widget widget_text">

                        <div class="textwidget">

                            <p>
                              <span class="text-highlight">TRACKSY</span>
                              2016 &copy; All rights reserved
                            </p>

                        </div><!-- end .textwidget -->

                    </div><!-- end .widget_text -->

                </div><!-- end .span6 -->
                <div class="span6" id="footer-bottom-widget-area-2">

                    <div class="widget widget_pages">

                        <ul>
                          <li class="page_item current_page_item"><a href="index.php">Home</a></li>
                            <!--li class="page_item"><a href="#">Home</a></li-->
                            <li class="page_item"><a href="index.php?page=features">Features</a></li>
                            <li class="page_item"><a href="index.php?page=integration">Integration</a></li>
                            <!--li class="page_item"><a href="#">Blog</a></li-->
                            <li class="page_item"><a href="index.php?page=contact">Contact</a></li>
                        </ul>

                    </div><!-- end .widget_pages -->

                </div><!-- end .span6 -->
            </div><!-- end .row -->
        </div><!-- end .container -->

<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

</div><!-- end #footer-bottom -->

</div><!-- end #wrap -->

<a id="back-to-top" href="#">
  <i class="ifc-up4"></i>
</a>

<!-- /// jQuery ////////  -->
<script src="assets/vendors/jquery-2.1.4.min.js"></script>

<!-- /// ViewPort ////////  -->
<script src="assets/vendors/viewport/jquery.viewport.js"></script>

<!-- /// Easing ////////  -->
<script src="assets/vendors/easing/jquery.easing.1.3.js"></script>

<!-- /// SimplePlaceholder ////////  -->
<script src="assets/vendors/simpleplaceholder/jquery.simpleplaceholder.js"></script>

<!-- /// Fitvids ////////  -->
<script src="assets/vendors/fitvids/jquery.fitvids.js"></script>

<!-- /// Animations ////////  -->
<script src="assets/vendors/animations/animate.js"></script>

<!-- /// Superfish Menu ////////  -->
<script src="assets/vendors/superfish/hoverIntent.js"></script>
<script src="assets/vendors/superfish/superfish.js"></script>

<!-- /// Revolution Slider ////////  -->
<script src="assets/vendors/revolutionslider/js/jquery.themepunch.tools.min.js"></script>
<script src="assets/vendors/revolutionslider/js/jquery.themepunch.revolution.min.js"></script>

<!-- /// bxSlider ////////  -->
<script src="assets/vendors/bxslider/jquery.bxslider.min.js"></script>

<!-- /// Magnific Popup ////////  -->
<script src="assets/vendors/magnificpopup/jquery.magnific-popup.min.js"></script>

<!-- /// Isotope ////////  -->
<script src="assets/vendors/isotope/imagesloaded.pkgd.min.js"></script>
<script src="assets/vendors/isotope/isotope.pkgd.min.js"></script>

<!-- /// Parallax ////////  -->
<script src="assets/vendors/parallax/jquery.parallax.min.js"></script>

<!-- /// EasyPieChart ////////  -->
<script src="assets/vendors/easypiechart/jquery.easypiechart.min.js"></script>

<!-- /// YTPlayer ////////  -->
<script src="assets/vendors/itplayer/jquery.mb.YTPlayer.js"></script>

<!-- /// Easy Tabs ////////  -->
<!--<script src="assets/vendors/easytabs/jquery.easytabs.min.js"></script>-->

<!-- /// Form validate ////////  -->
<script src="assets/vendors/jqueryvalidate/jquery.validate.min.js"></script>

<!-- /// Form submit ////////  -->
<script src="assets/vendors/jqueryform/jquery.form.min.js"></script>

<!-- /// gMap ////////  -->
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="assets/vendors/gmap/jquery.gmap.min.js"></script>

<!-- /// Twitter ////////  -->
<script src="assets/vendors/twitter/twitterfetcher.js"></script>

<!-- /// Custom JS ////////  -->
<script src="assets/js/main.js"></script>
