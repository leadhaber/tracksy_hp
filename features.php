<!-- /// CONTENT  /////////////////////////////////////////////////////////////////////////////////////////////////////////// -->



        <div class="container">
            <div class="row">
                <div class="span12">

        <div class="headline">

                        <h2>Features</h2>

                    </div><!-- end .headline -->

                </div><!-- end .span12 -->
            </div><!-- end .row -->
        </div><!-- end .container -->

<div class="container">

    <div class="row">
        <div class="span12">

            <h3 class="text-highlight">Break the crystal ball with tracksy powertools</h3>
            <p><h5>Tracksy.net offers real time tracking tools that give every click a meaning.
              With precise analytics and powerful visualization options you can adjust your business strategy and monitor your success.</h5></p>
        </div><!-- end .span12 -->
    </div><!-- end .row -->
<div class="divider single-line"></div>
    <div class="row">
        <div class="span4">

              <div class="box box-style-2">
                  <h4><li>Real Time Analytics</li></h4>
                  <p>Get instant access to everything that is going on. Get to know your users and create custom reports about every metric regarding your service and its users.</p>
              </div>
        </div>
        <div class="span8">
            <p>
                <a class="magnificPopup" href="content/portfolio/Traffic_Average.png">
                    <img class="dynamicimage" src="content/portfolio/Traffic_Average.png" alt="">
                </a>
            </p>

        </div>
    </div>
<div class="divider single-line"></div>
    <div class="row">
        <div class="span8">
            <p>
                <a class="magnificPopup" href="content/portfolio/heatmap.png">
                    <img class="dynamicimage" src="content/portfolio/heatmap.png" alt="">
                </a>
            </p>

        </div>
        <div class="span4">
            <div class="box box-style-2">
                <h4><li>Cursor Heatmap</li></h4>
                <p>Visualize the way customers are interacting with your service. Find out which sections are most engaging and how
                  userflow and navigation really work. Optimize usability, design and advertising with the help of these insights.</p>
            </div>

        </div>
    </div>
<div class="divider single-line"></div>
    <div class="row">
        <div class="span4">

              <div class="box box-style-2">
                  <h4><li>User Flow Charts</li></h4>
                  <p>Follow your users on their way through your service and see where they bounce off. See where they come from and where they are going, detect weak spots in your user flow
                    and keep your customers on board.</p>
              </div>
        </div>
        <div class="span8">
            <p>
                <a class="magnificPopup" href="content/portfolio/userflow.png">
                    <img class="dynamicimage" src="content/portfolio/userflow.png" alt="">
                </a>
            </p>

        </div>
    </div>
<div class="divider single-line"></div>
    <div class="row">
        <div class="span8">
            <p>
                <a class="magnificPopup" href="content/portfolio/Issue_Management.png">
                    <img class="dynamicimage" src="content/portfolio/Issue_Management.png" alt="">
                </a>
            </p>
        </div>
        <div class="span4">
            <div class="box box-style-2">
                <h4><li>Issue Management</li></h4>
                <p>Long loading times and compatibility issues can drive users away. Get a better understanding for your customers by gaining access to
                problems they are confronted with.</p>
            </div>
        </div>
    </div>

                  <a class="btn" style="margin-top:40px" href="index.php?page=contact">Get tracksy now!</a>

            </div>
            <br>



      </div><!-- end .container -->

<br />
<br />
<br />




<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

</div><!-- end #content -->
