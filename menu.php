<?php
$page = "";
if(isset($_GET["page"]))
    $page = $_GET["page"];

function navi($name, $url, $ref)
{
    global $page;
    if($page == $ref)
    {
        echo '<li class="dropdown current">
                <a href="'.$url.'"><span>'.$name.'</span></a>
              </li>';
    }
    else
    {
        echo '<li class="dropdown">
                <a href="'.$url.'"><span>'.$name.'</span></a>
              </li>';
    }
}
?>

<div id="header-wrap">
  <div id="header">

  <!-- /// HEADER  //////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

    <div class="container">
                <div class="row">
                    <div class="span3">

                        <!-- // Logo // -->
                        <div id="logo">
                            <a href="index.php">
                                <img src="assets/images/tracksy1.png" alt="tracksy" style="margin-top:20px">
                            </a>
                        </div>

                    </div><!-- end .span3 -->
                    <div class="span9">

                        <!-- // Mobile menu trigger // -->

                        <a href="#" id="mobile-menu-trigger">
                            <i class="fa fa-bars"></i>
                        </a>

                        <!-- // Menu // -->

                        <nav>
                            <ul class="sf-menu fixed" id="menu">

                                <?php
                                navi("Home",            "index.php",                    "");
                                navi("Features",        "index.php?page=features",      "features");
                                navi("Integration",     "index.php?page=integration",   "integration");
                                navi("Extensions",      "index.php?page=extensions",    "extensions");
                                navi("Contact",         "index.php?page=contact",       "contact");
                                navi("Login",           "index.php?page=login",         "login");
                                ?>

                            </ul>
                        </nav>

                    </div><!-- end .span9 -->
                </div><!-- end .row -->
    </div><!-- end .container -->

        <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  </div><!-- end #header -->
</div><!-- end #header-wrap -->
