<?php

if($page == "")
{
	include 'home.php';
}
else if($page == "features")
{
	include 'features.php';
}
else if($page == "integration")
{
	include 'integration.php';
}
else if($page == "extensions")
{
	include 'extensions.php';
}
else if($page == "contact")
{
	include 'contact.php';
}
else if($page == "login")
{
	include 'login.php';
}
else
{
	echo 'Ungültiger Link';
}
?>
