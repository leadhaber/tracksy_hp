
<!-- Begin Head-Bereich-->
<div class="fullwidth-section custom-color-selected-1" style="padding:0px 0">

                <div class="fullwidth-section-content">

                	<div class="container">
                        <div class="row">
                            <div class="span12">

                                <h1 class="headline">Real time user tracking, real insights!</h1>

                                    <h3 class="text-center text-uppercase h3-color-1">Grow your business with the tracking solution for market research professionals</h3>
                                <p class="text-center">For Web Entrepreneurs it is vital to understand how users interact with their services. With insights into user behavior it is possible to plan business strategies, optimize usability and increase traffic and sales.
                                  Tracksy.net is a service that provides an efficient user tracking algorithm for websites, shops, software services and more. Discover user behavior with the help of powerful statistics that allow you to scale your business and understand what your users really want.</p>

                            </div><!-- end .span12 -->
                        </div><!-- end .row -->
                    </div><!-- end .container -->

                </div><!-- end .fullwidth-section-content -->

            </div>
<!--End Head-Bereich-->

<!-- Three Aspects Begin -->

            <div class="container">
                            <div class="row">
                                <div class="span4">

                                    <div class="icon-box-1">

                                        <i class="ifc-user_female"></i>

                                        <div class="icon-box-content">

                                            <h5>
                                                <a href="single-service.html">Real User Behavior</a>
                                            </h5>

                                            <p>Tracksy.net analizes user behavior in detail and gives you access to powerful analytics tools that help you to monitor user interaction with your software or web service.</p>

                                        </div><!-- end .icon-box-content -->

                                    </div><!-- end .icon-box-2 -->

                                </div><!-- end .span4 -->
                                <div class="span4">

                                    <div class="icon-box-1">

                                        <i class="ifc-add_database"></i>

                                        <div class="icon-box-content">

                                            <h5>
                                                <a href="single-service.html">Real Time Data</a>
                                            </h5>

                                            <p>All data is being displayed in real time. That way you can see impact of your actions right away in the tracksy interface.</p>

                                        </div><!-- end .icon-box-content -->

                                    </div><!-- end .icon-box-2 -->

                                </div><!-- end .span4 -->
                                <div class="span4">

                                    <div class="icon-box-1">

                                        <i class="ifc-brain_filled"></i>

                                        <div class="icon-box-content">

                                            <h5>
                                                <a href="single-service.html">Real Insights</a>
                                            </h5>

                                            <p>Gather knowledge and get to know your users. Optimize your userflow and marketing actions with the help of tracksy.net.</p>

                                        </div><!-- end .icon-box-content -->

                                    </div><!-- end .icon-box-2 -->

                                </div><!-- end .span4 -->
                            </div><!-- end .row -->
                        </div>
<!-- Three Aspects End -->
&nbsp;
<!-- Begin 3er Menü-->
<div class="container">
                <div class="row">
                    <div class="span12">

                        <div class="vertical-tabs-container" data-easytabs="true">

                            <ul class="tabs-menu">
                                <li class="active">
                                    <a href="#content-tab-2-1" class="active">Easy integration</a>
                                </li>
                                <li class="">
                                    <a href="#content-tab-2-2" class="">Powerful Tracking</a>
                                </li>
                                <li class="">
                                    <a href="#content-tab-2-3" class="">Secure and anonymous</a>
                                </li>
                            </ul><!-- end .tabs-menu -->

                            <div class="tabs">

                                <div class="tab-content active" id="content-tab-2-1" style="display: block;">

                                    <p>Get started right away! Get your API key on tracksy.net and integrate the tracksy code into your web service or software.
                                      It is super easy. The tracksy developer team will offer support so you can smoothly integrate tracksy code into your product.</p>

                                </div>

                                <div class="tab-content" id="content-tab-2-2" style="display: none; position: static; visibility: visible;">

                                    <p>Gather information about user online time, GEO data, clicking behavior, mouse scrolling, refferer reports and much more.
                                      All data is provided in real time and will be visualized in convenient charts and graphs, live action heat maps or in depth detailed data logs.
                                    </p>

                                </div>

                                <div class="tab-content" id="content-tab-2-3" style="display: none; position: static; visibility: visible;">

                                    <p>Tracksy is all about data security and therefore operating within the highest data security standards.
                                      All data is being encrypted and 100% anonymous so the users privacy is ensured at all times. </p>

                                </div>

                            </div><!-- end .tabs -->

                        </div><!-- end .vertical-tabs-container -->

                    </div><!-- end .span12 -->
                </div><!-- end .row -->
            </div>
<!-- End 3er-Menü-->

<script>
var activeTab = 1;

$(".tabs-menu li").click(function(){
    $(".tabs-menu li").each(function(){
        $(this).attr("class", "");
    });
    $(".tabs div").each(function(){
        $(this).css({'display': 'none', 'visibility': 'hidden'});
    });
    $(this).attr("class", "active");
    $($(this).children("a").attr("href")).css({'display': 'block', 'visibility': 'visible'});
})
</script>

<br>
<br>
<br>
