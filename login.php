<?php
if(isset($_POST['btn-login']))
{
	echo '<div class="container">
			<div class="row">
		    	<div class="span12">
	    			<div style="color: red; margin-bottom: 20px; padding: 10px; text-align: center; border-bottom: 3px solid #EEE;;">
	    				Incorrect login. Please try it again.
					</div>
				</div>
			</div>
		</div>';
}
?>

<!-- /// CONTENT  /////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div class="container">
	<div class="row">
    	<div class="span12">

            <form name="example-form" method="post" action="#">
				<fieldset>

					<label for="text-input">Username</label>
					<input type="text" id="text-input" name="text-input" value="" placeholder="Enter your username">
                    <label for="password-input">Password</label>
					<input type="password" id="password-input" name="password-input" value="" placeholder="**********">

					<input type="submit" id="button-input" name="btn-login" value="Login">

				</fieldset>
			</form>

		</div>
	</div>
</div>