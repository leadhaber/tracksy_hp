
<!-- /// CONTENT  /////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div class="container">
  <div class="row">
      <div class="span12">

            <div class="headline">

                <h2>Tracksy for Browser Addons</h2>

            </div><!-- end .headline -->

        </div><!-- end .spsn12 -->
    </div><!-- end .row -->
</div><!-- end .container -->



<div class="container">
  <div class="row">

      <div class="span8">

          <h2 class="text-highlight">Implement tracksy for extensions in your browser addon <br /> and get all the insights about your extension performance!</h2>

          <p><b style="font-weight:bold;">If you are a developer for browser addons and extensions, tracksy offers an extension analytics service that gets you
              all the information you need about your extension performance. It works for all Mozilla Firefox, Google Chrome and Safari extensions.
          </b></p>
            <ul class="arrow">
            <li>Track installs and uninstalls to monitor your marketing activities and see how your extension is performing.</li>
            <li>Measure user activity to see how your active your extension users really are.</li>
            <li>Preserve the data privacy of your extension users by using an analytics solution that is not only anonymous, but targeted towards extension
            behavior only. Tracksy for extensions does not track user browsing and therefore is 100% compliant with the terms, conditions and privacy policies
            of the extension webstores!</li>
            </ul>

            <p></p>

<br>

<a class="btn" href="index.php?page=contact">Get tracksy for extensions!</a>

        </div><!-- end .row -->
        <div class="span4">
            <img src="assets/images/browsers.png">
        </div>


</div><!-- end .container -->

<br />

<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

</div><!-- end #content -->
