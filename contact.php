<!-- /// CONTENT  /////////////////////////////////////////////////////////////////////////////////////////////////////////// -->


        <div class="container">
          <div class="row">
              <div class="span12">

                    <div class="headline">

                        <h2>Sign up here!</h2>

                    </div><!-- end .headline -->

                </div><!-- end .spsn12 -->
            </div><!-- end .row -->
        </div><!-- end .container -->

        <div class="container">
          <div class="row">
              <div class="span9">

                    <h5 class="text-uppercase">Apply for tracksy!</h5>

                    <form id="contact-form" name="contact-form" method="post" action="assets/php/send.php">
                        <fieldset>
                            <div id="formstatus"></div>
                            <p>
                                <input class="span12" type="text" id="name" name="name" value="" placeholder="Name">
                            </p>
                            <p>
                                <input class="span12" type="text" id="subject" name="subject" value="" placeholder="Project Title">
                            </p>
                            <p>
                               <input class="span12" type="text" id="email" name="email" value="" placeholder="Email">
                            </p>
                            <p>
                                <textarea class="span12" id="message" name="message" rows="7" cols="25" placeholder="Please state the nature and URL of your software or service..."></textarea>
                            </p>
                            <p class="last">
                                <input class="btn" id="submit" type="submit" name="submit" value="Submit" >
                            </p>
                        </fieldset>
                    </form><!-- end #contact-form -->

            </div><!-- end .span9 -->
                <div class="span3">

                    <div class="widget ewf_widget_contact_info">

                        <h5 class="widget-title">Contact</h5>

                        <ul>
                            <li>
                                Get in touch to apply for tracksy.net!
                            </li>
                            <li>
                                <a href="mailto:info@tracksy.net">info@tracksy.net</a>
                            </li>

                        </ul>

                    </div><!-- end .ewf_widget_contact_info -->

            </div><!-- end .span3 -->
            </div><!-- end .row -->
        </div><!-- end .container -->

<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

</div><!-- end #content -->
