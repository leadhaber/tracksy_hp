
<!-- /// CONTENT  /////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div class="container">
  <div class="row">
      <div class="span12">

            <div class="headline">

                <h2>Integration</h2>

            </div><!-- end .headline -->

        </div><!-- end .spsn12 -->
    </div><!-- end .row -->
</div><!-- end .container -->



<div class="container">
  <div class="row">

      <div class="span8">

          <h2 class="text-highlight">Integrating tracksy is easy!</h2>

          <p><b style="font-weight:bold;">Get tracksy and start optimizing right now! All you need to do is follow these steps:</b></p>

            <p>1. Sign up for tracksy.net with your service and get your personal API key. You will receive a personal offer regarding your
            feature requests and traffic volume.</p>

        </div><!-- end .spsn8 -->
        <div class="span8">


            <p>2. Integrate the tracksy code snippet. It´s not hard at all but if you run into problems rest assured that our tech team has your back.</p>

            <p>3. Set up the tracksy interface. We have a preselection of great data visualizations to get you started. But of course it´s all up to you to create your custom data analysis interface.</p>
<br>
<br>

        </div><!-- end .spsn8 -->
        <div class="span4" style="margin-top:-100px">

              <div class="info-box-img" style="background-image:url(content/backgrounds/code3.png);"></div>

        </div><!-- end .span4 -->
</div><!-- end .row -->

    <a class="btn" style="margin-top:40px; margin-bottom:60px" href="index.php?page=contact">Get tracksy now!</a>

</div><!-- end .container -->



<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

</div><!-- end #content -->
